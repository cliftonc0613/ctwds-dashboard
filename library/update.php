<?php
/**
 * The remote host file to process update requests
 *
 */
if ( !isset( $_POST['action'] ) ) {
	echo '0';
	exit;
}

//set up the properties common to both requests
$obj = new stdClass();
$obj->slug = 'ctwds-dashboard.php';
$obj->name = 'CTWDS Dashboard';
$obj->plugin_name = 'ctwds-dashboard.php';
$obj->new_version = '1.0.1';
// the url for the plugin homepage
$obj->url = 'https://cliftonc0613@bitbucket.org/cliftonc0613/ctwds-dashboard.git';
//the download location for the plugin zip file (can be any internet host)
$obj->package = 'https://bitbucket.org/cliftonc0613/ctwds-dashboard/get/master.zip';

switch ( $_POST['action'] ) {

case 'version':
	echo serialize( $obj );
	break;
case 'info':
	$obj->requires = '4.0.0';
	$obj->tested = '4.7.4;
	$obj->downloaded = 3;
	$obj->last_updated = '2017-4-27';
	$obj->sections = array(
		'description' => 'This plugin adds a set of panels in the dashboard for the client.',
		'changelog' => 'first release of plugin'
	);
	$obj->download_link = $obj->package;
	echo serialize($obj);
case 'license':
	echo serialize( $obj );
	break;
}

?>
